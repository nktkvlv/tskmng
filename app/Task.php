<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Task extends Model
{
    protected $table = 'tasks';
    public $timestamps = false;

    protected $attributes = [
        'status' => '0',
    ];

    public static function add(Request $request) {
        $task = new Task;
        $task->title = $request->title;
        $task->description = $request->description;
        $task->created_at = date("Y-m-d H:i:s");
        $task->status = 0;
        $task->save();
    }

    public static function active() {
        $tasks = Task::where('status','=', 0)->paginate(5);
        return $tasks;
    }

    public static function complete() {
        $tasks = Task::where('status','=', 1)->paginate(5);
        return $tasks;
    }

    public static function completeTask($id) {
        $task = Task::find($id);
        $task->status = 1;
        $task->save();
    }

    public static function incompleteTask($id) {
        $task = Task::find($id);
        $task->status = 0;
        $task->save();
    }
}
