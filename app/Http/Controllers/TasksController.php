<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TasksController extends Controller
{

    public function addTask(Request $request) {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        else {
            Task::add($request);
            return redirect('/tasks');

        }
    }

    public function addTaskForm(Request $request) {
        $tasks = Task::complete();

        return view('add_tasks', compact('add_tasks'));
    }

    public function activeTasks(Request $request) {
        $tasks = Task::active();

        return view('tasks', compact('tasks'));
    }

    public function completeTasks(Request $request) {
        $tasks = Task::complete();

        return view('tasks', compact('tasks'));
    }

    public function deleteTask(Request $request, $id) {
        $tasks = Task::find($id);
        $tasks->delete();

        return redirect()->back();
    }

    public function updateForm(Request $request, $id) {
        $task = Task::find($id);
        return view('update_task_form', compact('task'));
    }
    public function updateTask(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }
        else {
            $task = Task::find($id);
            $task->title = $request->title;
            $task->description = $request->description;
            $task->save();
            return redirect('/tasks');

        }
        return view('update_task_form', compact('task'));
    }

    public function completeTask($id) {
        Task::completeTask($id);
        return redirect()->back();
    }

    public function incompleteTask($id) {
        Task::incompleteTask($id);
        return redirect()->back();
    }

}
