<?php

use Illuminate\Http\Request;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/tasks', 'TasksController@activeTasks');
Route::get('/tasks_completed', 'TasksController@completeTasks');
Route::post('/tasks', 'TasksController@addTask');
Route::get('/add_task_form', 'TasksController@addTaskForm');


Route::get('/task_complete/{task}', 'TasksController@completeTask');
Route::get('/task_incomplete/{task}', 'TasksController@incompleteTask');
Route::delete('/task_delete/{task}', 'TasksController@deleteTask');
Route::match(['get', 'put'], 'update_task_form/{task}', 'TasksController@updateForm');
Route::post('/update_task/{task}', 'TasksController@updateTask');

