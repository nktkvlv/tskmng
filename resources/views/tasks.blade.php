@extends('layouts.basic_template')

@section('content')

@include('errors')

@push('head')
    <script src="/js/edit_modal.js"></script>
@endpush


<div class="card-heading">
    @include('add_tasks')
    <a href="/tasks">Current tasks</a>
    <a href="/tasks_completed">Completed tasks</a>
</div>

    @if(count($tasks) > 0)
            <div class="card-heading">
            </div>
            <div class="card-body">
                <table class="table table-striped task-table">
                    <thead>
                        <tr class="d-flex">
                            <th>№</th>
                            <th class = "col-sm-2">Task</th>
                            <th class = "col-sm-7">Description</th>
                            <th class = "col-sm-1">&nbsp;</th>
                            <th class = "col-sm-1">&nbsp;</th>
                         </tr>
                    </thead>
                    <tbody>
                        @foreach($tasks as $task)
                            <tr class="d-flex">
                                <td class = "table-text">
                                    <div>
                                        {{ $task->id }}
                                    </div>
                                </td>
                                <td class = "table-text col-sm-2">
                                    <div>
                                        <a href= {{ url( 'task_update/' . $task->id) }}> {{ $task->title }}</a>
                                    </div>
                                </td>
                                <td class = "table-text col-sm-6">
                                    <div>
                                        {{ $task->description }}
                                    </div>
                                </td>
                                <td class = "table-text col-sm-1">
                                    <button class="btn btn-warning btn-detail open_modal_edit" value="{{$task->id}}">Edit</button>
                                </td>
                                @if(Route::current()->uri() === 'tasks')
                                    <td class = "col-sm-1">
                                        <form action = "{{ url( 'task_complete/' . $task->id) }}" method = "POST">
                                            {{ csrf_field() }}
                                            {{ method_field('GET') }}
                                            <button class="btn btn-success">Complete</button>
                                        </form>
                                    </td>
                                @endif
                                @if(Route::current()->uri() === 'tasks_completed')
                                    <td class = "col-sm-1">
                                        <form action = "{{ url( 'task_incomplete/' . $task->id) }}" method = "POST">
                                            {{ csrf_field() }}
                                            {{ method_field('GET') }}
                                            <button class="btn btn-warning">Rerun</button>
                                        </form>
                                    </td>
                                @endif
                                <td class = "col-sm-1">
                                    <form action = "{{ url( 'task_delete/' . $task->id) }}" method = "POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                         @endforeach
                    </tbody>
                </table>
                {{ $tasks->links() }}
            </div>
        </div>
    @endif
@endsection


